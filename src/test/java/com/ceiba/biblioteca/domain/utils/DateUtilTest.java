package com.ceiba.biblioteca.domain.utils;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DateUtilTest {

    @ParameterizedTest
    @CsvSource({"3,4,6", "6,1,2", "7,5,5", "1,3,3", "2,3,3", "4,10,14", "5,8,12"})
    void withDaysSkippingWeekendsSuccessfully(int dayOfWeek, int days, int addedDays) {
        LocalDate date = LocalDate.now().with(TemporalAdjusters.previous(DayOfWeek.of(dayOfWeek)));
        LocalDate actual = DateUtil.addDaysSkippingWeekends(date, days);
        assertEquals(date.plusDays(addedDays), actual);
    }

}
