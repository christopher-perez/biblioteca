package com.ceiba.biblioteca.domain.usecases.loans;

import com.ceiba.biblioteca.domain.exceptions.ResourceNotFoundException;
import com.ceiba.biblioteca.domain.models.loans.Book;
import com.ceiba.biblioteca.domain.repositories.BookRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static com.ceiba.biblioteca.domain.utils.DomainConstants.RECORD_NOT_FOUND;
import static com.ceiba.biblioteca.domain.utils.DomainConstants.RESOURCE_NOT_FOUND;
import static com.ceiba.biblioteca.utils.Utils.getBook;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class BookUseCaseTest {

    @Mock
    private BookRepository bookRepository;

    @InjectMocks
    private BookUseCaseImpl bookUseCase;

    @Test()
    public void whenBookNotFoundByIdReturnException() {
        long id = 7;

        when(bookRepository.findById(id))
                .thenReturn(Optional.empty());

        ResourceNotFoundException exception = assertThrows(ResourceNotFoundException.class, () -> {
            bookUseCase.findById(id);
        });

        assertEquals(String.format(RESOURCE_NOT_FOUND, id), exception.getMessage());
        verify(bookRepository, times(1)).findById(anyLong());
    }

    @Test()
    public void whenBookExistByIdReturnSuccess() {
        long id = 4;
        Book repositoryBook = getBook();

        when(bookRepository.findById(id))
                .thenReturn(Optional.of(repositoryBook));

        Book book = bookUseCase.findById(id);

        verify(bookRepository, times(1)).findById(anyLong());
        assertEquals(repositoryBook.getBookId(), book.getBookId());
        assertEquals(repositoryBook.getIsbn(), book.getIsbn());
        assertEquals(repositoryBook.getTitle(), book.getTitle());
    }

    @Test()
    public void whenBookNotFoundByIsbnReturnException() {
        String isbn = "HJN123";

        when(bookRepository.findByIsbn(isbn))
                .thenReturn(Optional.empty());

        ResourceNotFoundException exception = assertThrows(ResourceNotFoundException.class, () -> {
            bookUseCase.findByIsbn(isbn);
        });

        assertEquals(String.format(RECORD_NOT_FOUND, "Libro", "isbn", isbn), exception.getMessage());
        verify(bookRepository, times(1)).findByIsbn(anyString());
    }

    @Test()
    public void whenBookExistByIsbnReturnSuccess() {
        String isbn = "HJWSU12";
        Book repositoryBook = getBook();

        when(bookRepository.findByIsbn(isbn))
                .thenReturn(Optional.of(repositoryBook));

        Book book = bookUseCase.findByIsbn(isbn);

        verify(bookRepository, times(1)).findByIsbn(anyString());
        assertEquals(repositoryBook.getBookId(), book.getBookId());
        assertEquals(repositoryBook.getIsbn(), book.getIsbn());
        assertEquals(repositoryBook.getTitle(), book.getTitle());
    }

}
