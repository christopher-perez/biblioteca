package com.ceiba.biblioteca.domain.usecases.users;

import com.ceiba.biblioteca.domain.exceptions.ResourceNotFoundException;
import com.ceiba.biblioteca.domain.models.users.User;
import com.ceiba.biblioteca.domain.repositories.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static com.ceiba.biblioteca.domain.utils.DomainConstants.RECORD_NOT_FOUND;
import static com.ceiba.biblioteca.domain.utils.DomainConstants.RESOURCE_NOT_FOUND;
import static com.ceiba.biblioteca.utils.Utils.getUser;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UserUseCaseTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserUseCaseImpl userUseCase;

    @Test()
    public void whenUserNotFoundReturnException() {
        long id = 7;

        when(userRepository.findById(id))
                .thenReturn(Optional.empty());

        ResourceNotFoundException exception = assertThrows(ResourceNotFoundException.class, () -> {
            userUseCase.findById(id);
        });

        assertEquals(String.format(RESOURCE_NOT_FOUND, id), exception.getMessage());
        verify(userRepository, times(1)).findById(anyLong());
    }

    @Test()
    public void whenUserExistReturnSuccess() {
        long id = 4;
        User repositoryUser = getUser();

        when(userRepository.findById(id))
                .thenReturn(Optional.of(repositoryUser));

        User user = userUseCase.findById(id);

        verify(userRepository, times(1)).findById(anyLong());
        assertEquals(repositoryUser.getUserId(), user.getUserId());
        assertEquals(repositoryUser.getNames(), user.getNames());
        assertEquals(repositoryUser.getSurnames(), user.getSurnames());
        assertEquals(repositoryUser.getIdentificationNumber(), user.getIdentificationNumber());
    }

    @Test()
    public void whenUserNotFoundByIsbnReturnException() {
        String dni = "1001232123";

        when(userRepository.findByIdentificationNumber(dni))
                .thenReturn(Optional.empty());

        ResourceNotFoundException exception = assertThrows(ResourceNotFoundException.class, () -> {
            userUseCase.findByIdentificationNumber(dni);
        });

        assertEquals(String.format(RECORD_NOT_FOUND, "Usuario", "dni", dni), exception.getMessage());
        verify(userRepository, times(1)).findByIdentificationNumber(anyString());
    }

    @Test()
    public void whenUserExistByIsbnReturnSuccess() {
        String dni = "100123778";
        User repositoryUser = getUser();

        when(userRepository.findByIdentificationNumber(dni))
                .thenReturn(Optional.of(repositoryUser));

        User user = userUseCase.findByIdentificationNumber(dni);

        verify(userRepository, times(1)).findByIdentificationNumber(anyString());
        assertEquals(repositoryUser.getUserId(), user.getUserId());
        assertEquals(repositoryUser.getNames(), user.getNames());
        assertEquals(repositoryUser.getSurnames(), user.getSurnames());
        assertEquals(repositoryUser.getIdentificationNumber(), user.getIdentificationNumber());
    }

}
