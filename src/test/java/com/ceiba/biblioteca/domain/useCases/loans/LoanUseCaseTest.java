package com.ceiba.biblioteca.domain.usecases.loans;

import com.ceiba.biblioteca.domain.dtos.request.LoanSaveRequest;
import com.ceiba.biblioteca.domain.dtos.response.LoanResponse;
import com.ceiba.biblioteca.domain.dtos.response.LoanSaveResponse;
import com.ceiba.biblioteca.domain.exceptions.CustomException;
import com.ceiba.biblioteca.domain.exceptions.ResourceNotFoundException;
import com.ceiba.biblioteca.domain.models.loans.Loan;
import com.ceiba.biblioteca.domain.models.rules.Rule;
import com.ceiba.biblioteca.domain.repositories.LoanRepository;
import com.ceiba.biblioteca.domain.usecases.rules.RuleUseCase;
import com.ceiba.biblioteca.domain.usecases.users.UserTypeUseCase;
import com.ceiba.biblioteca.domain.usecases.users.UserUseCase;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static com.ceiba.biblioteca.domain.utils.DomainConstants.MAXIMUM_LOAN_QUOTA_MSG;
import static com.ceiba.biblioteca.domain.utils.DomainConstants.RESOURCE_NOT_FOUND;
import static com.ceiba.biblioteca.utils.Utils.getBook;
import static com.ceiba.biblioteca.utils.Utils.getLoan;
import static com.ceiba.biblioteca.utils.Utils.getRequest;
import static com.ceiba.biblioteca.utils.Utils.getRule;
import static com.ceiba.biblioteca.utils.Utils.getUser;
import static com.ceiba.biblioteca.utils.Utils.getUserType;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class LoanUseCaseTest {

    @Mock
    private LoanRepository loanRepository;

    @Mock
    private BookUseCase bookUseCase;

    @Mock
    private RuleUseCase ruleUseCase;

    @Mock
    private UserUseCase userUseCase;

    @Mock
    private UserTypeUseCase userTypeUseCase;

    @InjectMocks
    private LoanUseCaseImpl loanUseCase;


    @Test()
    public void whenUserTypeHaveManyLoansReturnException() {
        LoanSaveRequest request = getRequest();
        int loans = 5;
        Rule rule = getRule();
        rule.setMaximumLoans(loans);

        when(userTypeUseCase.findById(anyInt())).thenReturn(getUserType());
        when(ruleUseCase.findByUserTypeId(anyInt())).thenReturn(rule);
        when(bookUseCase.findByIsbn(anyString())).thenReturn(getBook());
        when(userUseCase.findByIdentificationNumber(anyString())).thenReturn(getUser());
        when(loanRepository.numberActiveLoans(getUser().getUserId())).thenReturn(loans);

        CustomException exception = assertThrows(CustomException.class, () -> {
            loanUseCase.save(request);
        });

        verify(userTypeUseCase, times(1)).findById(anyInt());
        verify(ruleUseCase, times(1)).findByUserTypeId(anyInt());
        verify(bookUseCase, times(1)).findByIsbn(anyString());
        verify(userUseCase, times(1)).findByIdentificationNumber(anyString());
        verify(loanRepository, times(1)).numberActiveLoans(getUser().getUserId());
        verify(loanRepository, times(0)).save(any());
        assertEquals(String.format(MAXIMUM_LOAN_QUOTA_MSG, getUser().getIdentificationNumber()), exception.getMessage());
    }

    @Test()
    public void whenUserTypeHaveSomeLoansReturnSuccess() {
        LoanSaveRequest request = getRequest();
        int loans = 3;
        Rule rule = getRule();
        rule.setMaximumLoans(loans * 2);

        when(userTypeUseCase.findById(anyInt())).thenReturn(getUserType());
        when(ruleUseCase.findByUserTypeId(anyInt())).thenReturn(rule);
        when(bookUseCase.findByIsbn(anyString())).thenReturn(getBook());
        when(userUseCase.findByIdentificationNumber(anyString())).thenReturn(getUser());
        when(loanRepository.numberActiveLoans(getUser().getUserId())).thenReturn(loans);
        when(loanRepository.save(any(Loan.class))).thenReturn(getLoan());

        LoanSaveResponse saveResponse = loanUseCase.save(request);

        verify(userTypeUseCase, times(1)).findById(anyInt());
        verify(ruleUseCase, times(1)).findByUserTypeId(anyInt());
        verify(bookUseCase, times(1)).findByIsbn(anyString());
        verify(userUseCase, times(1)).findByIdentificationNumber(anyString());
        verify(loanRepository, times(1)).numberActiveLoans(getUser().getUserId());
        verify(loanRepository, times(1)).save(any());

        assertEquals(getLoan().getLoanId(), saveResponse.getId());
        assertEquals(getLoan().getReturnDate(), saveResponse.getFechaMaximaDevolucion());
    }

    @Test()
    public void whenLoanNotFoundReturnException() {
        int id = 7;

        when(loanRepository.findById(id))
                .thenReturn(Optional.empty());

        ResourceNotFoundException exception = assertThrows(ResourceNotFoundException.class, () -> {
            loanUseCase.findById(id);
        });

        assertEquals(String.format(RESOURCE_NOT_FOUND, id), exception.getMessage());
        verify(loanRepository, times(1)).findById(anyInt());
    }

    @Test()
    public void whenLoanExistReturnSuccess() {
        int id = 4;
        Loan repositoryLoan = getLoan();

        when(loanRepository.findById(id))
                .thenReturn(Optional.of(repositoryLoan));

        LoanResponse loan = loanUseCase.findById(id);

        verify(loanRepository, times(1)).findById(anyInt());
        assertEquals(repositoryLoan.getLoanId(), loan.getId());
        assertEquals(repositoryLoan.getUser().getIdentificationNumber(), loan.getIdentificacionUsuario());
        assertEquals(repositoryLoan.getReturnDate(), loan.getFechaMaximaDevolucion());
        assertEquals(repositoryLoan.getBook().getIsbn(), loan.getIsbn());
        assertEquals(repositoryLoan.getUserType().getTypeId(), loan.getTipoUsuario());
    }

}
