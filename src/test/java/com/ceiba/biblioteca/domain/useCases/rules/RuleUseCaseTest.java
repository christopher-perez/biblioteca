package com.ceiba.biblioteca.domain.usecases.rules;

import com.ceiba.biblioteca.domain.exceptions.ResourceNotFoundException;
import com.ceiba.biblioteca.domain.models.rules.Rule;
import com.ceiba.biblioteca.domain.repositories.RuleRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static com.ceiba.biblioteca.domain.utils.DomainConstants.RECORD_NOT_FOUND;
import static com.ceiba.biblioteca.domain.utils.DomainConstants.RESOURCE_NOT_FOUND;
import static com.ceiba.biblioteca.utils.Utils.getRule;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class RuleUseCaseTest {

    @Mock
    private RuleRepository ruleRepository;

    @InjectMocks
    private RuleUseCaseImpl ruleUseCase;

    @Test()
    public void whenRuleNotFoundReturnException() {
        long id = 8;

        when(ruleRepository.findById(id))
                .thenReturn(Optional.empty());

        ResourceNotFoundException exception = assertThrows(ResourceNotFoundException.class, () -> {
            ruleUseCase.findById(id);
        });

        assertEquals(String.format(RESOURCE_NOT_FOUND, id), exception.getMessage());
        verify(ruleRepository, times(1)).findById(anyLong());
    }

    @Test()
    public void whenRuleExistReturnSuccess() {
        long id = 4;
        Rule repositoryRule = getRule();

        when(ruleRepository.findById(id))
                .thenReturn(Optional.of(repositoryRule));

        Rule rule = ruleUseCase.findById(id);

        verify(ruleRepository, times(1)).findById(anyLong());
        assertEquals(repositoryRule.getRuleId(), rule.getRuleId());
        assertEquals(repositoryRule.getMaximumDaysLoan(), rule.getMaximumDaysLoan());
        assertEquals(repositoryRule.getMaximumLoans(), rule.getMaximumLoans());
        assertEquals(repositoryRule.getUserType().getTypeId(), rule.getUserType().getTypeId());
    }

    @Test()
    public void whenRuleNotFoundByUserTypeReturnException() {
        int userId = 3;

        when(ruleRepository.findByUserType(userId))
                .thenReturn(Optional.empty());

        ResourceNotFoundException exception = assertThrows(ResourceNotFoundException.class, () -> {
            ruleUseCase.findByUserTypeId(userId);
        });

        assertEquals(String.format(RECORD_NOT_FOUND, "Regla", "tipo usuario", userId), exception.getMessage());
        verify(ruleRepository, times(1)).findByUserType(anyInt());
    }

    @Test()
    public void whenRuleExistByUserTypeReturnSuccess() {
        int userId = 8;
        Rule repositoryRule = getRule();

        when(ruleRepository.findByUserType(userId))
                .thenReturn(Optional.of(repositoryRule));

        Rule rule = ruleUseCase.findByUserTypeId(userId);

        verify(ruleRepository, times(1)).findByUserType(anyInt());
        assertEquals(repositoryRule.getRuleId(), rule.getRuleId());
        assertEquals(repositoryRule.getMaximumDaysLoan(), rule.getMaximumDaysLoan());
        assertEquals(repositoryRule.getMaximumLoans(), rule.getMaximumLoans());
        assertEquals(repositoryRule.getUserType().getTypeId(), rule.getUserType().getTypeId());
    }

}
