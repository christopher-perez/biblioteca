package com.ceiba.biblioteca.domain.usecases.users;

import com.ceiba.biblioteca.domain.exceptions.ResourceNotFoundException;
import com.ceiba.biblioteca.domain.models.users.UserType;
import com.ceiba.biblioteca.domain.repositories.UserTypeRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static com.ceiba.biblioteca.domain.utils.DomainConstants.USER_TYPE_NOT_FOUND;
import static com.ceiba.biblioteca.utils.Utils.getUserType;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UserTypeUseCaseTest {

    @Mock
    private UserTypeRepository userTypeRepository;

    @InjectMocks
    private UserTypeUseCaseImpl userTypeUseCase;

    @Test()
    public void whenUserTypeNotFoundReturnException() {
        int id = 2;

        when(userTypeRepository.findById(id))
                .thenReturn(Optional.empty());

        ResourceNotFoundException exception = assertThrows(ResourceNotFoundException.class, () -> {
            userTypeUseCase.findById(id);
        });

        assertEquals(USER_TYPE_NOT_FOUND, exception.getMessage());
        verify(userTypeRepository, times(1)).findById(anyInt());
    }

    @Test()
    public void whenUserTypeExistReturnSuccess() {
        int id = 7;
        UserType repositoryUserType = getUserType();

        when(userTypeRepository.findById(id))
                .thenReturn(Optional.of(repositoryUserType));

        UserType userType = userTypeUseCase.findById(id);

        verify(userTypeRepository, times(1)).findById(anyInt());
        assertEquals(repositoryUserType.getTypeId(), userType.getTypeId());
        assertEquals(repositoryUserType.getName(), userType.getName());
    }

}
