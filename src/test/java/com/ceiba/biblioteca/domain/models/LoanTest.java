package com.ceiba.biblioteca.domain.models;

import com.ceiba.biblioteca.domain.dtos.enums.LoanState;
import com.ceiba.biblioteca.domain.models.loans.Loan;
import com.ceiba.biblioteca.domain.utils.DateUtil;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

import java.time.LocalDate;

import static com.ceiba.biblioteca.utils.Utils.getBook;
import static com.ceiba.biblioteca.utils.Utils.getUser;
import static com.ceiba.biblioteca.utils.Utils.getUserType;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;

public class LoanTest {

    @Test
    void whenNewInitializeAll() {
        LocalDate dateExpected = LocalDate.now().plusDays(5);

        try (MockedStatic<DateUtil> utilities = Mockito.mockStatic(DateUtil.class)) {
            utilities.when(() -> DateUtil.addDaysSkippingWeekends(any(), anyInt()))
                    .thenReturn(dateExpected);

            Loan loan = new Loan(getBook(), getUser(), getUserType(), 10);

            utilities.verify(() -> DateUtil.addDaysSkippingWeekends(any(), anyInt()),
                    times(1));

            assertEquals(dateExpected, loan.getReturnDate());
            assertEquals(LoanState.ACTIVE, loan.getState());
            assertNotNull(loan.getBook());
            assertNotNull(loan.getUser());
            assertNotNull(loan.getUserType());
        }
    }

}
