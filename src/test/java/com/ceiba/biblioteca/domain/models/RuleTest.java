package com.ceiba.biblioteca.domain.models;

import com.ceiba.biblioteca.domain.models.rules.Rule;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class RuleTest {

    @Test
    void whenNewInitializeAll() {
        Rule rule = new Rule();
        assertNotNull(rule.getUserType());
    }

}
