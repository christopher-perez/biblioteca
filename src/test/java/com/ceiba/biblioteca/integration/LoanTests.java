package com.ceiba.biblioteca.integration;

import com.ceiba.biblioteca.ResultadoPrestarTest;
import com.ceiba.biblioteca.SolicitudPrestarLibroTest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static com.ceiba.biblioteca.domain.utils.DateUtil.addDaysSkippingWeekends;
import static org.hamcrest.core.Is.is;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@AutoConfigureMockMvc
class LoanTests {

    public static final int USUARIO_AFILIADO = 1;
    public static final int USUARIO_EMPLEADO = 2;
    public static final int USUARIO_INVITADO = 3;

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void whenBookNotExistShouldException() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                        .post("/prestamo")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(getRequest("HHJ12UI", "74851254", USUARIO_EMPLEADO)))
                .andDo(print())
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.mensaje").exists())
                .andExpect(jsonPath("$.mensaje", is("Libro con isbn HHJ12UI no ha sido encontrado")));
    }

    @Test
    void whenUserNotExistShouldException() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                        .post("/prestamo")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(getRequest("EQWQW8545", "8481545", USUARIO_AFILIADO)))
                .andDo(print())
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.mensaje").exists())
                .andExpect(jsonPath("$.mensaje", is("Usuario con dni 8481545 no ha sido encontrado")));
    }

    @Test
    void loansSuccessWhenGuestHaveLoansFinished() throws Exception {

        MvcResult result = mvc.perform(MockMvcRequestBuilders
                        .post("/prestamo")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(getRequest("EQWQW8545", "102567123", USUARIO_INVITADO)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.fechaMaximaDevolucion").exists())
                .andReturn();

        ResultadoPrestarTest resultLoan = objectMapper.readValue(
                result.getResponse().getContentAsString(), ResultadoPrestarTest.class);

        LocalDate returnDate = LocalDate.now();
        returnDate = addDaysSkippingWeekends(returnDate, 7);
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd/MM/yyyy");

        mvc.perform(MockMvcRequestBuilders
                        .get("/prestamo/" + resultLoan.getId())
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.fechaMaximaDevolucion", is(returnDate.format(format))))
                .andExpect(jsonPath("$.isbn", is("EQWQW8545")))
                .andExpect(jsonPath("$.identificacionUsuario", is("102567123")))
                .andExpect(jsonPath("$.tipoUsuario", is(USUARIO_INVITADO)));
    }

    @Test
    void loansErrorWhenGuestHaveLoansExpired() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                        .post("/prestamo")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(getRequest("EQWQW8545", "102567189", USUARIO_INVITADO)))
                .andDo(print())
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.mensaje", is("El usuario con identificación 102567189 ya tiene un libro prestado por lo cual no se le puede realizar otro préstamo")));
    }

    private String getRequest(String isbn, String dni, int userType) throws JsonProcessingException {
        return objectMapper.writeValueAsString(
                new SolicitudPrestarLibroTest(isbn, dni, userType));
    }

}
