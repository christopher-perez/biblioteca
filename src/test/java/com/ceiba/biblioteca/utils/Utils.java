package com.ceiba.biblioteca.utils;

import com.ceiba.biblioteca.domain.dtos.request.LoanSaveRequest;
import com.ceiba.biblioteca.domain.dtos.response.LoanResponse;
import com.ceiba.biblioteca.domain.dtos.response.LoanSaveResponse;
import com.ceiba.biblioteca.domain.models.loans.Book;
import com.ceiba.biblioteca.domain.models.loans.Loan;
import com.ceiba.biblioteca.domain.models.rules.Rule;
import com.ceiba.biblioteca.domain.models.users.User;
import com.ceiba.biblioteca.domain.models.users.UserType;
import com.ceiba.biblioteca.infrastructure.adapters.loans.book.BookData;
import com.ceiba.biblioteca.infrastructure.adapters.loans.loan.LoanData;
import com.ceiba.biblioteca.infrastructure.adapters.loans.mappers.LoansMapper;
import com.ceiba.biblioteca.infrastructure.adapters.rules.mappers.RulesMapper;
import com.ceiba.biblioteca.infrastructure.adapters.rules.rule.RuleData;
import com.ceiba.biblioteca.infrastructure.adapters.users.mappers.UsersMapper;
import com.ceiba.biblioteca.infrastructure.adapters.users.type.UserTypeData;
import com.ceiba.biblioteca.infrastructure.adapters.users.user.UserData;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.time.LocalDate;

public class Utils {

    public static LoanSaveResponse getResponseSaveOk() {
        return new LoanSaveResponse();
    }

    public static LoanResponse getResponseOk() {
        return new LoanResponse();
    }

    public static LoanSaveRequest getRequest() {
        LoanSaveRequest loanSaveRequest = new LoanSaveRequest();
        loanSaveRequest.setIsbn("ad2213");
        loanSaveRequest.setIdentificacionUsuario("10012123");
        loanSaveRequest.setTipoUsuario(2);
        return loanSaveRequest;
    }

    public static LoanData getLoanData() {
        LoanData loanData = new LoanData();
        loanData.setLoanId(1);
        loanData.setBook(getBookData());
        loanData.setUser(getUserData());
        loanData.setType(getUserTypeData());
        loanData.setReturnDate(LocalDate.now().plusDays(7));
        return loanData;
    }

    public static Loan getLoan() {
        Loan loan = new Loan();
        loan.setLoanId(1);
        loan.setBook(getBook());
        loan.setUser(getUser());
        loan.setUserType(getUserType());
        loan.setReturnDate(LocalDate.now().plusDays(7));
        return loan;
    }

    public static Book getBook() {
        Book book = new Book();
        book.setBookId(1L);
        book.setIsbn("HJ1239as");
        return book;
    }

    public static BookData getBookData() {
        return LoansMapper.bookEntityToData(getBook());
    }

    public static User getUser() {
        User user = new User();
        user.setUserId(1L);
        user.setIdentificationNumber("10012312");
        return user;
    }

    public static UserData getUserData() {
        return UsersMapper.userEntityToData(getUser());
    }

    public static UserType getUserType() {
        UserType userType = new UserType();
        userType.setTypeId(1);
        return userType;
    }

    public static UserTypeData getUserTypeData() {
        return UsersMapper.typeEntityToData(getUserType());
    }

    public static Rule getRule() {
        Rule rule = new Rule();
        rule.setRuleId(1L);
        rule.setUserType(getUserType());
        rule.setMaximumLoans(12);
        rule.setMaximumDaysLoan(9);
        return rule;
    }

    public static RuleData getRuleData() {
        return RulesMapper.ruleEntityToData(getRule());
    }

    public static String asJsonString(Object object) {
        try {
            return new ObjectMapper().writeValueAsString(object);
        } catch (Exception e) {
            throw new RuntimeException();
        }
    }


}
