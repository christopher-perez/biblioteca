package com.ceiba.biblioteca.utils;

public class ClassTest {

    private String field;

    public ClassTest(String field) {
        this.field = field;
    }

    public ClassTest() {

    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }
}
