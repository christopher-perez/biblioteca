package com.ceiba.biblioteca.application.utils;

import io.github.cdimascio.dotenv.Dotenv;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class DotEnvUtilTest {

    @Test
    void testToGetDotenv() {
        Dotenv dotenv = DotenvUtil.getInstance();
        Assertions.assertNotNull(dotenv);
        Dotenv dotenvInstance = DotenvUtil.getInstance();
        Assertions.assertSame(dotenv, dotenvInstance);
    }

}
