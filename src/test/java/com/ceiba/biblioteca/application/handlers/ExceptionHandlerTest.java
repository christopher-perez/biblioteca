package com.ceiba.biblioteca.application.handlers;

import com.ceiba.biblioteca.domain.dtos.ErrorResponse;
import com.ceiba.biblioteca.domain.dtos.ExceptionResponse;
import com.ceiba.biblioteca.domain.exceptions.CustomException;
import com.ceiba.biblioteca.domain.exceptions.ResourceNotFoundException;
import com.ceiba.biblioteca.utils.ClassTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;

import static com.ceiba.biblioteca.application.utils.Constants.GENERAL_ERROR_MESSAGE;
import static com.ceiba.biblioteca.application.utils.Constants.REQUEST_NOT_READABLE;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class ExceptionHandlerTest {

    @InjectMocks
    private GlobalExceptionHandler globalExceptionHandler;

    @Test
    void handleAllExceptionsTest() {
        ResponseEntity<Object> response = globalExceptionHandler.handleAllExceptions(
                new Exception("Han error occurred"));
        ErrorResponse responseBody = (ErrorResponse) response.getBody();

        assertEquals(GENERAL_ERROR_MESSAGE, responseBody.getMessages().get(0));
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
    }

    @Test
    void resourceNotFoundTest() {
        String messageExpected = "Resource not found error";

        ResponseEntity<ExceptionResponse> response = globalExceptionHandler.resourceNotFound(
                new ResourceNotFoundException(messageExpected));
        ExceptionResponse responseBody = response.getBody();

        assertEquals(messageExpected, responseBody.getMensaje());
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());

    }

    @Test
    void customExceptionTest() {
        String messageExpected = "Business error";

        ResponseEntity<ExceptionResponse> response = globalExceptionHandler.customException(new CustomException(messageExpected));
        ExceptionResponse responseBody = response.getBody();

        assertEquals(messageExpected, responseBody.getMensaje());
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    void notReadableExceptionTest() {
        ResponseEntity<ErrorResponse> response = globalExceptionHandler.notReadableException(null);
        ErrorResponse responseBody = response.getBody();

        assertEquals(REQUEST_NOT_READABLE, responseBody.getMessages().get(0));
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());

    }

    @Test
    void handleMethodArgumentNotValidTest() {
        ResponseEntity<Object> response;
        response = globalExceptionHandler.handleMethodArgumentNotValid(getMethodArgumentNotValidException());
        ErrorResponse responseBody = (ErrorResponse) response.getBody();

        assertEquals("BAD_REQUEST", responseBody.getErrorCode());
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    private MethodArgumentNotValidException getMethodArgumentNotValidException() {
        BeanPropertyBindingResult errors
                = new BeanPropertyBindingResult(new ClassTest(), "testClass");

        errors.rejectValue("field", "BAD");
        return new MethodArgumentNotValidException(null, errors);
    }

}
