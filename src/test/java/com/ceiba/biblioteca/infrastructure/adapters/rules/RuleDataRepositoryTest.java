package com.ceiba.biblioteca.infrastructure.adapters.rules;

import com.ceiba.biblioteca.domain.models.rules.Rule;
import com.ceiba.biblioteca.infrastructure.adapters.rules.rule.RuleData;
import com.ceiba.biblioteca.infrastructure.adapters.rules.rule.RuleDataJPA;
import com.ceiba.biblioteca.infrastructure.adapters.rules.rule.RuleDataRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static com.ceiba.biblioteca.utils.Utils.getRuleData;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.anyInt;
import static org.mockito.Mockito.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class RuleDataRepositoryTest {

    @Mock
    private RuleDataJPA repositoryJPA;

    @InjectMocks
    private RuleDataRepository repository;

    @Test
    void whenFindRuleByIdReturnSuccess() {
        long id = 3;
        RuleData expectedResponse = getRuleData();

        when(repositoryJPA.findById(id)).thenReturn(Optional.of(expectedResponse));

        Rule ruleResponse = repository.findById(id).get();

        verify(repositoryJPA, times(1)).findById(anyLong());
        assertEquals(expectedResponse.getRuleId(), ruleResponse.getRuleId());
        assertEquals(expectedResponse.getMaximumDaysLoan(), ruleResponse.getMaximumDaysLoan());
        assertEquals(expectedResponse.getMaximumLoans(), ruleResponse.getMaximumLoans());
        assertEquals(expectedResponse.getType().getTypeId(), ruleResponse.getUserType().getTypeId());
    }

    @Test
    void whenFindRuleByIdNotFoundShouldEmpty() {
        long id = 9;

        when(repositoryJPA.findById(id)).thenReturn(Optional.empty());

        Optional<Rule> ruleResponse = repository.findById(id);

        verify(repositoryJPA, times(1)).findById(anyLong());
        assertEquals(Optional.empty(), ruleResponse);
    }

    @Test
    void whenFindRuleByIsbnReturnSuccess() {
        Integer userTypeId = 8;
        RuleData expectedResponse = getRuleData();

        when(repositoryJPA.findByTypeTypeId(userTypeId)).thenReturn(Optional.of(expectedResponse));

        Rule ruleResponse = repository.findByUserType(userTypeId).get();

        verify(repositoryJPA, times(1)).findByTypeTypeId(anyInt());
        assertEquals(expectedResponse.getRuleId(), ruleResponse.getRuleId());
        assertEquals(expectedResponse.getMaximumDaysLoan(), ruleResponse.getMaximumDaysLoan());
        assertEquals(expectedResponse.getMaximumLoans(), ruleResponse.getMaximumLoans());
        assertEquals(expectedResponse.getType().getTypeId(), ruleResponse.getUserType().getTypeId());
    }

    @Test
    void whenFindRuleByIsbnNotFoundShouldEmpty() {
        Integer userTypeId = 3;

        when(repositoryJPA.findByTypeTypeId(userTypeId)).thenReturn(Optional.empty());

        Optional<Rule> ruleResponse = repository.findByUserType(userTypeId);

        verify(repositoryJPA, times(1)).findByTypeTypeId(anyInt());
        assertEquals(Optional.empty(), ruleResponse);
    }
}
