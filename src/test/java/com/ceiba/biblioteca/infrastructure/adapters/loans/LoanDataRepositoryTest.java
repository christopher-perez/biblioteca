package com.ceiba.biblioteca.infrastructure.adapters.loans;

import com.ceiba.biblioteca.domain.dtos.enums.LoanState;
import com.ceiba.biblioteca.domain.models.loans.Loan;
import com.ceiba.biblioteca.infrastructure.adapters.loans.loan.LoanData;
import com.ceiba.biblioteca.infrastructure.adapters.loans.loan.LoanDataJPA;
import com.ceiba.biblioteca.infrastructure.adapters.loans.loan.LoanDataRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static com.ceiba.biblioteca.utils.Utils.getLoan;
import static com.ceiba.biblioteca.utils.Utils.getLoanData;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class LoanDataRepositoryTest {

    @Mock
    private LoanDataJPA repositoryJPA;

    @InjectMocks
    private LoanDataRepository repository;

    @Test
    void whenUserSaveShouldSuccessfully() {
        Loan loanRequest = getLoan();

        when(repositoryJPA.save(any())).thenReturn(getLoanData());
        Loan loanResponse = repository.save(loanRequest);

        verify(repositoryJPA, times(1)).save(any(LoanData.class));
        assertEquals(loanRequest.getLoanId(), loanResponse.getLoanId());
        assertEquals(loanRequest.getBook(), loanResponse.getBook());
        assertEquals(loanRequest.getUser(), loanResponse.getUser());
        assertEquals(loanRequest.getUserType(), loanResponse.getUserType());
        assertEquals(loanRequest.getReturnDate(), loanResponse.getReturnDate());
    }

    @Test
    void whenFindLoanReturnSuccess() {
        int id = 3;
        LoanData expectedResponse = getLoanData();

        when(repositoryJPA.findById(id)).thenReturn(Optional.of(expectedResponse));

        Loan loanResponse = repository.findById(id).get();

        verify(repositoryJPA, times(1)).findById(anyInt());
        assertEquals(expectedResponse.getLoanId(), loanResponse.getLoanId());
        assertEquals(expectedResponse.getBook().getBookId(), loanResponse.getBook().getBookId());
        assertEquals(expectedResponse.getBook().getIsbn(), loanResponse.getBook().getIsbn());
        assertEquals(expectedResponse.getUser().getUserId(), loanResponse.getUser().getUserId());
        assertEquals(expectedResponse.getUser().getIdentificationNumber(), loanResponse.getUser().getIdentificationNumber());
        assertEquals(expectedResponse.getType().getTypeId(), loanResponse.getUserType().getTypeId());
        assertEquals(expectedResponse.getReturnDate(), loanResponse.getReturnDate());
    }

    @Test
    void whenLoanNotFoundShouldEmpty() {
        int id = 9;

        when(repositoryJPA.findById(id)).thenReturn(Optional.empty());

        Optional<Loan> loanResponse = repository.findById(id);

        verify(repositoryJPA, times(1)).findById(anyInt());
        assertEquals(Optional.empty(), loanResponse);
    }

    @Test
    void shouldReturnNumberActiveLoans() {
        Long userId = 2L;
        int numberLoans = 6;

        when(repositoryJPA.countAllByUserUserIdAndStateIsNot(userId, LoanState.FINALIZED)).thenReturn(numberLoans);

        Integer response = repository.numberActiveLoans(userId);

        verify(repositoryJPA, times(1)).countAllByUserUserIdAndStateIsNot(userId, LoanState.FINALIZED);
        assertEquals(numberLoans, response);
    }

}
