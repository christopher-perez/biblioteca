package com.ceiba.biblioteca.infrastructure.adapters.loans;

import com.ceiba.biblioteca.domain.models.loans.Book;
import com.ceiba.biblioteca.infrastructure.adapters.loans.book.BookData;
import com.ceiba.biblioteca.infrastructure.adapters.loans.book.BookDataJPA;
import com.ceiba.biblioteca.infrastructure.adapters.loans.book.BookDataRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static com.ceiba.biblioteca.utils.Utils.getBookData;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.anyLong;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class BookDataRepositoryTest {

    @Mock
    private BookDataJPA repositoryJPA;

    @InjectMocks
    private BookDataRepository repository;

    @Test
    void whenFindBookByIdReturnSuccess() {
        long id = 3;
        BookData expectedResponse = getBookData();

        when(repositoryJPA.findById(id)).thenReturn(Optional.of(expectedResponse));

        Book bookResponse = repository.findById(id).get();

        verify(repositoryJPA, times(1)).findById(anyLong());
        assertEquals(expectedResponse.getBookId(), bookResponse.getBookId());
        assertEquals(expectedResponse.getIsbn(), bookResponse.getIsbn());
        assertEquals(expectedResponse.getTitle(), bookResponse.getTitle());
    }

    @Test
    void whenFindBookByIdNotFoundShouldEmpty() {
        long id = 9;

        when(repositoryJPA.findById(id)).thenReturn(Optional.empty());

        Optional<Book> bookResponse = repository.findById(id);

        verify(repositoryJPA, times(1)).findById(anyLong());
        assertEquals(Optional.empty(), bookResponse);
    }

    @Test
    void whenFindBookByIsbnReturnSuccess() {
        String isbn = "AH12311K21J";
        BookData expectedResponse = getBookData();

        when(repositoryJPA.findByIsbn(isbn)).thenReturn(Optional.of(expectedResponse));

        Book bookResponse = repository.findByIsbn(isbn).get();

        verify(repositoryJPA, times(1)).findByIsbn(anyString());
        assertEquals(expectedResponse.getBookId(), bookResponse.getBookId());
        assertEquals(expectedResponse.getIsbn(), bookResponse.getIsbn());
        assertEquals(expectedResponse.getTitle(), bookResponse.getTitle());
    }

    @Test
    void whenFindBookByIsbnNotFoundShouldEmpty() {
        String isbn = "AH12311KJ";

        when(repositoryJPA.findByIsbn(isbn)).thenReturn(Optional.empty());

        Optional<Book> bookResponse = repository.findByIsbn(isbn);

        verify(repositoryJPA, times(1)).findByIsbn(anyString());
        assertEquals(Optional.empty(), bookResponse);
    }

}
