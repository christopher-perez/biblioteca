package com.ceiba.biblioteca.infrastructure.adapters.users;

import com.ceiba.biblioteca.domain.models.users.UserType;
import com.ceiba.biblioteca.infrastructure.adapters.users.type.UserTypeData;
import com.ceiba.biblioteca.infrastructure.adapters.users.type.UserTypeDataJPA;
import com.ceiba.biblioteca.infrastructure.adapters.users.type.UserTypeDataRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static com.ceiba.biblioteca.utils.Utils.getUserTypeData;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class TypeDataRepositoryTest {

    @Mock
    private UserTypeDataJPA repositoryJPA;

    @InjectMocks
    private UserTypeDataRepository repository;

    @Test
    void whenFindUserTypeByIdReturnSuccess() {
        int id = 3;
        UserTypeData expectedResponse = getUserTypeData();

        when(repositoryJPA.findById(id)).thenReturn(Optional.of(expectedResponse));

        UserType userTypeResponse = repository.findById(id).get();

        verify(repositoryJPA, times(1)).findById(anyInt());
        assertEquals(expectedResponse.getTypeId(), userTypeResponse.getTypeId());
        assertEquals(expectedResponse.getName(), userTypeResponse.getName());
    }

    @Test
    void whenFindUserTypeByIdNotFoundShouldEmpty() {
        int id = 9;

        when(repositoryJPA.findById(id)).thenReturn(Optional.empty());

        Optional<UserType> userTypeResponse = repository.findById(id);

        verify(repositoryJPA, times(1)).findById(anyInt());
        assertEquals(Optional.empty(), userTypeResponse);
    }

}
