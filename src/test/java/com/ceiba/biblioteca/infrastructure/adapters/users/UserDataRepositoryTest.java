package com.ceiba.biblioteca.infrastructure.adapters.users;

import com.ceiba.biblioteca.domain.models.users.User;
import com.ceiba.biblioteca.infrastructure.adapters.users.user.UserData;
import com.ceiba.biblioteca.infrastructure.adapters.users.user.UserDataJPA;
import com.ceiba.biblioteca.infrastructure.adapters.users.user.UserDataRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static com.ceiba.biblioteca.utils.Utils.getUserData;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.anyLong;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UserDataRepositoryTest {

    @Mock
    private UserDataJPA repositoryJPA;

    @InjectMocks
    private UserDataRepository repository;

    @Test
    void whenFindUserByIdReturnSuccess() {
        long id = 3;
        UserData expectedResponse = getUserData();

        when(repositoryJPA.findById(id)).thenReturn(Optional.of(expectedResponse));

        User userResponse = repository.findById(id).get();

        verify(repositoryJPA, times(1)).findById(anyLong());
        assertEquals(expectedResponse.getUserId(), userResponse.getUserId());
        assertEquals(expectedResponse.getNames(), userResponse.getNames());
        assertEquals(expectedResponse.getSurnames(), userResponse.getSurnames());
        assertEquals(expectedResponse.getIdentificationNumber(), userResponse.getIdentificationNumber());
    }

    @Test
    void whenFindUserByIdNotFoundShouldEmpty() {
        long id = 9;

        when(repositoryJPA.findById(id)).thenReturn(Optional.empty());

        Optional<User> userResponse = repository.findById(id);

        verify(repositoryJPA, times(1)).findById(anyLong());
        assertEquals(Optional.empty(), userResponse);
    }

    @Test
    void whenFindUserByIsbnReturnSuccess() {
        String dni = "100239123";
        UserData expectedResponse = getUserData();

        when(repositoryJPA.findByIdentificationNumber(dni)).thenReturn(Optional.of(expectedResponse));

        User userResponse = repository.findByIdentificationNumber(dni).get();

        verify(repositoryJPA, times(1)).findByIdentificationNumber(anyString());
        assertEquals(expectedResponse.getUserId(), userResponse.getUserId());
        assertEquals(expectedResponse.getNames(), userResponse.getNames());
        assertEquals(expectedResponse.getSurnames(), userResponse.getSurnames());
        assertEquals(expectedResponse.getIdentificationNumber(), userResponse.getIdentificationNumber());
    }

    @Test
    void whenFindUserByIsbnNotFoundShouldEmpty() {
        String dni = "990239123";

        when(repositoryJPA.findByIdentificationNumber(dni)).thenReturn(Optional.empty());

        Optional<User> userResponse = repository.findByIdentificationNumber(dni);

        verify(repositoryJPA, times(1)).findByIdentificationNumber(anyString());
        assertEquals(Optional.empty(), userResponse);
    }
}
