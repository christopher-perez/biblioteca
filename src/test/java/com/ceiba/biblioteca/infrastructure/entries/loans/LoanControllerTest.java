package com.ceiba.biblioteca.infrastructure.entries.loans;

import com.ceiba.biblioteca.domain.dtos.request.LoanSaveRequest;
import com.ceiba.biblioteca.domain.usecases.loans.LoanUseCase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static com.ceiba.biblioteca.utils.Utils.asJsonString;
import static com.ceiba.biblioteca.utils.Utils.getRequest;
import static com.ceiba.biblioteca.utils.Utils.getResponseOk;
import static com.ceiba.biblioteca.utils.Utils.getResponseSaveOk;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
public class LoanControllerTest {

    @Mock
    private LoanUseCase loanUseCase;

    @InjectMocks
    private LoanController loanController;

    @Autowired
    private MockMvc mockMvc;

    private final String baseUrl;

    public LoanControllerTest() {
        this.baseUrl = "/prestamo";
    }

    @BeforeEach
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(loanController).build();
    }

    @Test
    void saveLoanTest() throws Exception {
        when(loanUseCase.save(any(LoanSaveRequest.class))).thenReturn(getResponseSaveOk());
        mockMvc.perform(post(baseUrl)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(getRequest())))
                .andExpect(status().isOk())
                .andDo(print());
        verify(loanUseCase, times(1)).save(any());
    }

    @Test
    void getLoanSuccessfully() throws Exception {
        String url = baseUrl + "/" + 1;
        when(loanUseCase.findById(anyInt())).thenReturn(getResponseOk());
        mockMvc.perform(get(url)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(getResponseOk())))
                .andExpect(status().isOk())
                .andDo(print());
        verify(loanUseCase, times(1)).findById(anyInt());
    }

}
