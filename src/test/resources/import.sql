INSERT INTO USER_TYPES(type_id, name, created_at, updated_at) VALUES (1, 'AFILIADO', '2022-01-01', '2022-01-02');
INSERT INTO USER_TYPES(type_id, name, created_at, updated_at) VALUES (2, 'EMPLEADO', '2022-01-01', '2022-01-02');
INSERT INTO USER_TYPES(type_id, name, created_at, updated_at) VALUES (3, 'INVITADO', '2022-01-01', '2022-01-03');

INSERT INTO users(user_id, identification_number, names, surnames, created_at, updated_at) VALUES (1,'974148', 'Juan', 'Diaz', '2022-01-01', '2022-01-03');
INSERT INTO users(user_id, identification_number, names, surnames, created_at, updated_at) VALUES (2,'7481545', 'Pablo', 'Morsa', '2022-01-01', '2022-01-03');
INSERT INTO users(user_id, identification_number, names, surnames, created_at, updated_at) VALUES (3,'74851254', 'Mateo', 'Rios', '2022-01-01', '2022-01-03');
INSERT INTO users(user_id, identification_number, names, surnames, created_at, updated_at) VALUES (4, '1111111111', 'Maria', 'Suarez', '2022-01-01', '2022-01-03');
INSERT INTO users(user_id, identification_number, names, surnames, created_at, updated_at) VALUES (5, '102567123', 'Juana', 'De arco', '2022-01-01', '2022-01-03');
INSERT INTO users(user_id, identification_number, names, surnames, created_at, updated_at) VALUES (6, '102567189', 'Carlos', 'Cuartas', '2022-01-01', '2022-01-03');

INSERT INTO books(book_id, isbn, title, created_at, updated_at) VALUES (1,'ASDA7884', 'Cien años de soledad', '2022-01-01', '2022-01-01');
INSERT INTO books(book_id, isbn, title, created_at, updated_at) VALUES (2,'AWQ489', 'La odisea', '2022-01-01', '2022-01-01');
INSERT INTO books(book_id, isbn, title, created_at, updated_at) VALUES (3,'EQWQW8545', 'Quijote', '2022-01-01', '2022-01-01');

INSERT INTO rules(rule_id, type_id, max_loan_days, max_loans, created_at, updated_at) VALUES (1, 1, 10, 10, '2022-01-01', '2022-01-01');
INSERT INTO rules(rule_id, type_id, max_loan_days, max_loans, created_at, updated_at) VALUES (2, 2, 8, 10, '2022-01-01', '2022-01-01');
INSERT INTO rules(rule_id, type_id, max_loan_days, max_loans, created_at, updated_at) VALUES (3, 3, 7, 1, '2022-01-01', '2022-01-01');

INSERT INTO loans(loan_id, user_id, book_id, type_id, return_date, state, created_at, updated_at) VALUES (10, 5, 1, 3, '2022-01-10', 1,'2022-01-01', '2022-01-01');
INSERT INTO loans(loan_id, user_id, book_id, type_id, return_date, state, created_at, updated_at) VALUES (20, 5, 1, 3, '2022-01-10', 1,'2022-01-01', '2022-01-01');
INSERT INTO loans(loan_id, user_id, book_id, type_id, return_date, state, created_at, updated_at) VALUES (30, 6, 2, 3, '2022-01-11', 2,'2022-01-01', '2022-01-01');
INSERT INTO loans(loan_id, user_id, book_id, type_id, return_date, state, created_at, updated_at) VALUES (40, 6, 3, 3, '2022-01-08', 1,'2022-01-01', '2022-01-01');
