package com.ceiba.biblioteca.infrastructure.entries.loans;

import com.ceiba.biblioteca.domain.dtos.request.LoanSaveRequest;
import com.ceiba.biblioteca.domain.dtos.response.LoanResponse;
import com.ceiba.biblioteca.domain.dtos.response.LoanSaveResponse;
import com.ceiba.biblioteca.domain.usecases.loans.LoanUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/prestamo")
@RequiredArgsConstructor
@Validated
public class LoanController {

    private final LoanUseCase loanUseCase;

    @PostMapping("")
    public ResponseEntity<LoanSaveResponse> saveLoan(@Valid @RequestBody LoanSaveRequest request) {
        return ResponseEntity.ok(loanUseCase.save(request));
    }

    @GetMapping("/{id}")
    public ResponseEntity<LoanResponse> getLoan(@PathVariable Integer id) {
        return ResponseEntity.ok(loanUseCase.findById(id));
    }

}
