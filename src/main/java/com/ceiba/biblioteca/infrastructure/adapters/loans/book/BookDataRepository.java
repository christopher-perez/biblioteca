package com.ceiba.biblioteca.infrastructure.adapters.loans.book;

import com.ceiba.biblioteca.domain.models.loans.Book;
import com.ceiba.biblioteca.domain.repositories.BookRepository;
import com.ceiba.biblioteca.infrastructure.adapters.loans.mappers.LoansMapper;
import lombok.RequiredArgsConstructor;

import java.util.Optional;

@RequiredArgsConstructor
public class BookDataRepository implements BookRepository {
    private final BookDataJPA repositoryJPA;

    @Override
    public Optional<Book> findByIsbn(String isbn) {
        return repositoryJPA.findByIsbn(isbn)
                .map(LoansMapper::bookDataToEntity);
    }

    @Override
    public Optional<Book> findById(Long id) {
        return repositoryJPA.findById(id)
                .map(LoansMapper::bookDataToEntity);
    }
}
