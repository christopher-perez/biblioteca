package com.ceiba.biblioteca.infrastructure.adapters.users.mappers;

import com.ceiba.biblioteca.domain.models.users.User;
import com.ceiba.biblioteca.domain.models.users.UserType;
import com.ceiba.biblioteca.infrastructure.adapters.users.type.UserTypeData;
import com.ceiba.biblioteca.infrastructure.adapters.users.user.UserData;

public class UsersMapper {

    private UsersMapper(){
    }

    public static User userDataToEntity(UserData data) {
        User user = new User();
        user.setUserId(data.getUserId());
        user.setNames(data.getNames());
        user.setSurnames(data.getSurnames());
        user.setIdentificationNumber(data.getIdentificationNumber());
        user.setCreateAt(data.getCreateAt());
        user.setUpdateAt(data.getCreateAt());
        return user;
    }

    public static UserData userEntityToData(User entity) {
        UserData userData = new UserData();
        userData.setUserId(entity.getUserId());
        userData.setNames(entity.getNames());
        userData.setSurnames(entity.getSurnames());
        userData.setIdentificationNumber(entity.getIdentificationNumber());
        userData.setCreateAt(entity.getCreateAt());
        userData.setUpdateAt(entity.getUpdateAt());
        return userData;
    }

    public static UserType typeDataToEntity(UserTypeData data) {
        UserType userType = new UserType();
        userType.setTypeId(data.getTypeId());
        userType.setName(data.getName());
        userType.setCreateAt(data.getCreateAt());
        userType.setUpdateAt(data.getCreateAt());
        return userType;
    }

    public static UserTypeData typeEntityToData(UserType entity) {
        UserTypeData userTypeData = new UserTypeData();
        userTypeData.setTypeId(entity.getTypeId());
        userTypeData.setName(entity.getName());
        userTypeData.setCreateAt(entity.getCreateAt());
        userTypeData.setUpdateAt(entity.getUpdateAt());
        return userTypeData;
    }


}
