package com.ceiba.biblioteca.infrastructure.adapters.users.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserDataJPA extends JpaRepository<UserData, Long> {

    Optional<UserData> findByIdentificationNumber(String identificationNumber);

}
