package com.ceiba.biblioteca.infrastructure.adapters.users.user;

import com.ceiba.biblioteca.domain.models.users.User;
import com.ceiba.biblioteca.domain.repositories.UserRepository;
import com.ceiba.biblioteca.infrastructure.adapters.users.mappers.UsersMapper;
import lombok.RequiredArgsConstructor;

import java.util.Optional;

@RequiredArgsConstructor
public class UserDataRepository implements UserRepository {
    private final UserDataJPA repositoryJPA;

    @Override
    public Optional<User> findById(Long id) {
        return repositoryJPA.findById(id)
                .map(UsersMapper::userDataToEntity);
    }

    @Override
    public Optional<User> findByIdentificationNumber(String dni) {
        return repositoryJPA.findByIdentificationNumber(dni)
                .map(UsersMapper::userDataToEntity);
    }
}
