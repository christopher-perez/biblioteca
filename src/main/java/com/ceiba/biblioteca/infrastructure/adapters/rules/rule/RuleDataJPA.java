package com.ceiba.biblioteca.infrastructure.adapters.rules.rule;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RuleDataJPA extends JpaRepository<RuleData, Long> {

    Optional<RuleData> findByTypeTypeId(Integer userType);

}
