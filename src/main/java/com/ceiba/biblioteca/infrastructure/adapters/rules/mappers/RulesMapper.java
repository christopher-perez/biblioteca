package com.ceiba.biblioteca.infrastructure.adapters.rules.mappers;

import com.ceiba.biblioteca.domain.models.rules.Rule;
import com.ceiba.biblioteca.infrastructure.adapters.rules.rule.RuleData;
import com.ceiba.biblioteca.infrastructure.adapters.users.mappers.UsersMapper;

public class RulesMapper {

    private RulesMapper(){
    }

    public static Rule ruleDataToEntity(RuleData data) {
        Rule rule = new Rule();
        rule.setMaximumDaysLoan(data.getMaximumDaysLoan());
        rule.setMaximumLoans(data.getMaximumLoans());
        rule.setUserType(UsersMapper.typeDataToEntity(data.getType()));
        rule.setCreateAt(data.getCreateAt());
        rule.setUpdateAt(data.getCreateAt());
        return rule;
    }

    public static RuleData ruleEntityToData(Rule entity) {
        RuleData ruleData = new RuleData();
        ruleData.setMaximumLoans(entity.getMaximumLoans());
        ruleData.setMaximumDaysLoan(entity.getMaximumDaysLoan());
        ruleData.setType(UsersMapper.typeEntityToData(entity.getUserType()));
        ruleData.setCreateAt(entity.getCreateAt());
        ruleData.setUpdateAt(entity.getUpdateAt());
        return ruleData;
    }

}
