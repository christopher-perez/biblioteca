package com.ceiba.biblioteca.infrastructure.adapters.rules.rule;

import com.ceiba.biblioteca.domain.models.rules.Rule;
import com.ceiba.biblioteca.domain.repositories.RuleRepository;
import com.ceiba.biblioteca.infrastructure.adapters.rules.mappers.RulesMapper;
import lombok.RequiredArgsConstructor;

import java.util.Optional;

@RequiredArgsConstructor
public class RuleDataRepository implements RuleRepository {
    private final RuleDataJPA repositoryJPA;

    @Override
    public Optional<Rule> findByUserType(Integer userType) {
        return repositoryJPA.findByTypeTypeId(userType)
                .map(RulesMapper::ruleDataToEntity);
    }

    @Override
    public Optional<Rule> findById(Long id) {
        return repositoryJPA.findById(id)
                .map(RulesMapper::ruleDataToEntity);
    }
}
