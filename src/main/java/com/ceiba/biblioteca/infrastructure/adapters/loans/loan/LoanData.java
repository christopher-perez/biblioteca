package com.ceiba.biblioteca.infrastructure.adapters.loans.loan;

import com.ceiba.biblioteca.domain.dtos.enums.LoanState;
import com.ceiba.biblioteca.infrastructure.adapters.loans.book.BookData;
import com.ceiba.biblioteca.infrastructure.adapters.users.type.UserTypeData;
import com.ceiba.biblioteca.infrastructure.adapters.users.user.UserData;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name = "loans")
@Getter
@Setter
@RequiredArgsConstructor
public class LoanData implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "loan_id")
    private Integer loanId;

    @Column(name = "return_date")
    private LocalDate returnDate;

    private LoanState state;

    @Column(name = "created_at")
    private LocalDateTime createAt;
    @Column(name = "updated_at")
    private LocalDateTime updateAt;

    @ManyToOne()
    @JoinColumn(name = "book_id")
    private BookData book;

    @ManyToOne()
    @JoinColumn(name = "user_id")
    private UserData user;

    @ManyToOne()
    @JoinColumn(name = "type_id")
    private UserTypeData type;

    @PrePersist
    private void prePersistFunction() {
        this.createAt = LocalDateTime.now();
        this.updateAt = LocalDateTime.now();
    }

    @PreUpdate
    public void preUpdateFunction() {
        this.updateAt = LocalDateTime.now();
    }

}