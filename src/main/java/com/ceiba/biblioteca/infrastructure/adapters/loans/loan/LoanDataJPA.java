package com.ceiba.biblioteca.infrastructure.adapters.loans.loan;

import com.ceiba.biblioteca.domain.dtos.enums.LoanState;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LoanDataJPA extends JpaRepository<LoanData, Integer> {

    Integer countAllByUserUserIdAndStateIsNot(Long userId, LoanState state);

}
