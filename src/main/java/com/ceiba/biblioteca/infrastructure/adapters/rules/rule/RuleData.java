package com.ceiba.biblioteca.infrastructure.adapters.rules.rule;

import com.ceiba.biblioteca.infrastructure.adapters.users.type.UserTypeData;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "rules")
@Getter
@Setter
@RequiredArgsConstructor
public class RuleData implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "rule_id")
    private Long ruleId;

    @Column(name = "max_loan_days")
    private int maximumDaysLoan;

    @Column(name = "max_loans")
    private int maximumLoans;

    @Column(name = "created_at")
    private LocalDateTime createAt;
    @Column(name = "updated_at")
    private LocalDateTime updateAt;

    @ManyToOne()
    @JoinColumn(name = "type_id", unique = true)
    private UserTypeData type;

    @PrePersist
    private void prePersistFunction() {
        this.createAt = LocalDateTime.now();
        this.updateAt = LocalDateTime.now();
    }

    @PreUpdate
    public void preUpdateFunction() {
        this.updateAt = LocalDateTime.now();
    }

}