package com.ceiba.biblioteca.infrastructure.adapters.loans.loan;

import com.ceiba.biblioteca.domain.dtos.enums.LoanState;
import com.ceiba.biblioteca.domain.models.loans.Loan;
import com.ceiba.biblioteca.domain.repositories.LoanRepository;
import com.ceiba.biblioteca.infrastructure.adapters.loans.mappers.LoansMapper;
import lombok.RequiredArgsConstructor;

import java.util.Optional;

@RequiredArgsConstructor
public class LoanDataRepository implements LoanRepository {
    private final LoanDataJPA repositoryJPA;

    @Override
    public Loan save(Loan request) {
        return LoansMapper.loanDataToEntity(
                repositoryJPA.save(LoansMapper.loanEntityToData(request)));
    }

    @Override
    public Optional<Loan> findById(Integer id) {
        return repositoryJPA.findById(id)
                .map(LoansMapper::loanDataToEntity);
    }

    @Override
    public Integer numberActiveLoans(Long userId) {
        return repositoryJPA.countAllByUserUserIdAndStateIsNot(userId, LoanState.FINALIZED);
    }

}
