package com.ceiba.biblioteca.infrastructure.adapters.loans.mappers;

import com.ceiba.biblioteca.domain.models.loans.Book;
import com.ceiba.biblioteca.domain.models.loans.Loan;
import com.ceiba.biblioteca.infrastructure.adapters.loans.book.BookData;
import com.ceiba.biblioteca.infrastructure.adapters.loans.loan.LoanData;
import com.ceiba.biblioteca.infrastructure.adapters.users.mappers.UsersMapper;

public class LoansMapper {

    private LoansMapper(){
    }

    public static Loan loanDataToEntity(LoanData data) {
        Loan loan = new Loan();
        loan.setLoanId(data.getLoanId());
        loan.setBook(bookDataToEntity(data.getBook()));
        loan.setUserType(UsersMapper.typeDataToEntity(data.getType()));
        loan.setUser(UsersMapper.userDataToEntity(data.getUser()));
        loan.setReturnDate(data.getReturnDate());
        loan.setState(data.getState());
        loan.setCreateAt(data.getCreateAt());
        loan.setUpdateAt(data.getCreateAt());
        return loan;
    }

    public static LoanData loanEntityToData(Loan entity) {
        LoanData loanData = new LoanData();
        loanData.setLoanId(entity.getLoanId());
        loanData.setBook(bookEntityToData(entity.getBook()));
        loanData.setReturnDate(entity.getReturnDate());
        loanData.setUser(UsersMapper.userEntityToData(entity.getUser()));
        loanData.setState(entity.getState());
        loanData.setType(UsersMapper.typeEntityToData(entity.getUserType()));
        loanData.setCreateAt(entity.getCreateAt());
        loanData.setUpdateAt(entity.getUpdateAt());
        return loanData;
    }

    public static Book bookDataToEntity(BookData data) {
        Book book = new Book();
        book.setBookId(data.getBookId());
        book.setIsbn(data.getIsbn());
        book.setTitle(data.getTitle());
        book.setCreateAt(data.getCreateAt());
        book.setUpdateAt(data.getCreateAt());
        return book;
    }

    public static BookData bookEntityToData(Book entity) {
        BookData bookData = new BookData();
        bookData.setBookId(entity.getBookId());
        bookData.setIsbn(entity.getIsbn());
        bookData.setTitle(entity.getTitle());
        bookData.setCreateAt(entity.getCreateAt());
        bookData.setUpdateAt(entity.getUpdateAt());
        return bookData;
    }

}
