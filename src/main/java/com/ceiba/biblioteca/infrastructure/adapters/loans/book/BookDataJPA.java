package com.ceiba.biblioteca.infrastructure.adapters.loans.book;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BookDataJPA extends JpaRepository<BookData, Long> {

    Optional<BookData> findByIsbn(String isbn);

}
