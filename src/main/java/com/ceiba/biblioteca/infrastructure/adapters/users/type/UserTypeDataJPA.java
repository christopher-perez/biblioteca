package com.ceiba.biblioteca.infrastructure.adapters.users.type;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserTypeDataJPA extends JpaRepository<UserTypeData, Integer> {

}
