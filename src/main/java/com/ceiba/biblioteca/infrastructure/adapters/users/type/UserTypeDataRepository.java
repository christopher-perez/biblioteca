package com.ceiba.biblioteca.infrastructure.adapters.users.type;

import com.ceiba.biblioteca.domain.models.users.UserType;
import com.ceiba.biblioteca.domain.repositories.UserTypeRepository;
import com.ceiba.biblioteca.infrastructure.adapters.users.mappers.UsersMapper;
import lombok.RequiredArgsConstructor;

import java.util.Optional;

@RequiredArgsConstructor
public class UserTypeDataRepository implements UserTypeRepository {
    private final UserTypeDataJPA repositoryJPA;

    @Override
    public Optional<UserType> findById(Integer id) {
        return repositoryJPA.findById(id)
                .map(UsersMapper::typeDataToEntity);
    }
}
