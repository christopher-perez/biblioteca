package com.ceiba.biblioteca.domain.mappers;

import com.ceiba.biblioteca.domain.dtos.response.LoanResponse;
import com.ceiba.biblioteca.domain.dtos.response.LoanSaveResponse;
import com.ceiba.biblioteca.domain.models.loans.Loan;

public class LoanMapperDomain {

    private LoanMapperDomain(){

    }

    public static LoanSaveResponse entityToSaveResponse(Loan loanEntity) {
        LoanSaveResponse response = new LoanSaveResponse();
        response.setId(loanEntity.getLoanId());
        response.setFechaMaximaDevolucion(loanEntity.getReturnDate());
        return response;
    }

    public static LoanResponse entityToResponse(Loan loanEntity) {
        LoanResponse loanResponse = new LoanResponse();
        loanResponse.setId(loanEntity.getLoanId());
        loanResponse.setFechaMaximaDevolucion(loanEntity.getReturnDate());
        loanResponse.setIdentificacionUsuario(loanEntity.getUser().getIdentificationNumber());
        loanResponse.setIsbn(loanEntity.getBook().getIsbn());
        loanResponse.setTipoUsuario(loanEntity.getUserType().getTypeId());
        return loanResponse;
    }

}
