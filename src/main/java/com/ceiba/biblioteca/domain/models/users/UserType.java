package com.ceiba.biblioteca.domain.models.users;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class UserType {

    private Integer typeId;
    private String name;

    private LocalDateTime createAt;
    private LocalDateTime updateAt;
}
