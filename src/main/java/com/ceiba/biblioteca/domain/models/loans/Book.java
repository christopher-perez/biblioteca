package com.ceiba.biblioteca.domain.models.loans;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Book {

    private Long bookId;
    private String isbn;
    private String title;

    private LocalDateTime createAt;
    private LocalDateTime updateAt;
}
