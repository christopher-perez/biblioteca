package com.ceiba.biblioteca.domain.models.users;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class User {

    private Long userId;
    private String identificationNumber;
    private String names;
    private String surnames;

    private LocalDateTime createAt;
    private LocalDateTime updateAt;
}
