package com.ceiba.biblioteca.domain.models.loans;

import com.ceiba.biblioteca.domain.dtos.enums.LoanState;
import com.ceiba.biblioteca.domain.models.users.User;
import com.ceiba.biblioteca.domain.models.users.UserType;
import com.ceiba.biblioteca.domain.utils.DateUtil;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
public class Loan {

    private Integer loanId;
    private Book book;
    private User user;
    private UserType userType;
    private LocalDate returnDate;
    private LoanState state;

    private LocalDateTime createAt;
    private LocalDateTime updateAt;

    public Loan(Book book, User user, UserType userType, int days) {
        this.book = book;
        this.user = user;
        this.userType = userType;
        this.state = LoanState.ACTIVE;
        calculateReturnDate(days);
    }

    private void calculateReturnDate(int days) {
        LocalDate now = LocalDate.now();
        this.returnDate = DateUtil.addDaysSkippingWeekends(now, days);
    }

}