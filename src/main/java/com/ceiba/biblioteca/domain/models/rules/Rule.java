package com.ceiba.biblioteca.domain.models.rules;

import com.ceiba.biblioteca.domain.models.users.UserType;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Rule {

    private Long ruleId;
    private UserType userType;
    private int maximumDaysLoan;
    private int maximumLoans;

    private LocalDateTime createAt;
    private LocalDateTime updateAt;

    public Rule() {
        this.userType = new UserType();
    }
}
