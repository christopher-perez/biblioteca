package com.ceiba.biblioteca.domain.usecases.rules;

import com.ceiba.biblioteca.domain.exceptions.ResourceNotFoundException;
import com.ceiba.biblioteca.domain.models.rules.Rule;
import com.ceiba.biblioteca.domain.repositories.RuleRepository;
import com.ceiba.biblioteca.domain.utils.DomainConstants;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class RuleUseCaseImpl implements RuleUseCase {

    private final RuleRepository repository;

    @Override
    public Rule findById(Long id) {
        return repository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format(DomainConstants.RESOURCE_NOT_FOUND, id)));
    }

    @Override
    public Rule findByUserTypeId(Integer userType) {
        return repository.findByUserType(userType)
                .orElseThrow(() -> new ResourceNotFoundException(
                        String.format(DomainConstants.RECORD_NOT_FOUND, "Regla", "tipo usuario", userType)));
    }

}
