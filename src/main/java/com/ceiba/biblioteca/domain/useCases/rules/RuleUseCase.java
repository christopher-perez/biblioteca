package com.ceiba.biblioteca.domain.usecases.rules;

import com.ceiba.biblioteca.domain.models.rules.Rule;

public interface RuleUseCase {

    Rule findById(Long id);

    Rule findByUserTypeId(Integer userType);

}
