package com.ceiba.biblioteca.domain.usecases.users;

import com.ceiba.biblioteca.domain.exceptions.ResourceNotFoundException;
import com.ceiba.biblioteca.domain.models.users.UserType;
import com.ceiba.biblioteca.domain.repositories.UserTypeRepository;
import lombok.RequiredArgsConstructor;

import static com.ceiba.biblioteca.domain.utils.DomainConstants.USER_TYPE_NOT_FOUND;

@RequiredArgsConstructor
public class UserTypeUseCaseImpl implements UserTypeUseCase {

    private final UserTypeRepository repository;

    @Override
    public UserType findById(Integer id) {
        return repository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(USER_TYPE_NOT_FOUND));
    }

}
