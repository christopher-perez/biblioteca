package com.ceiba.biblioteca.domain.usecases.loans;

import com.ceiba.biblioteca.domain.models.loans.Book;

public interface BookUseCase {

    Book findById(Long id);

    Book findByIsbn(String isbn);

}
