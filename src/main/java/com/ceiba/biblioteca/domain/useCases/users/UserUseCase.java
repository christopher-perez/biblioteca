package com.ceiba.biblioteca.domain.usecases.users;

import com.ceiba.biblioteca.domain.models.users.User;

public interface UserUseCase {

    User findById(Long id);

    User findByIdentificationNumber(String isbn);

}
