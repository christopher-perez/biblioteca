package com.ceiba.biblioteca.domain.usecases.users;

import com.ceiba.biblioteca.domain.exceptions.ResourceNotFoundException;
import com.ceiba.biblioteca.domain.models.users.User;
import com.ceiba.biblioteca.domain.repositories.UserRepository;
import com.ceiba.biblioteca.domain.utils.DomainConstants;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class UserUseCaseImpl implements UserUseCase {

    private final UserRepository repository;

    @Override
    public User findById(Long id) {
        return repository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format(DomainConstants.RESOURCE_NOT_FOUND, id)));
    }

    @Override
    public User findByIdentificationNumber(String dni) {
        return repository.findByIdentificationNumber(dni)
                .orElseThrow(() -> new ResourceNotFoundException(
                        String.format(DomainConstants.RECORD_NOT_FOUND, "Usuario", "dni", dni)));
    }

}
