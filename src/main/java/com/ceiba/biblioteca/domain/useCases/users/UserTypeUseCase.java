package com.ceiba.biblioteca.domain.usecases.users;

import com.ceiba.biblioteca.domain.models.users.UserType;

public interface UserTypeUseCase {

    UserType findById(Integer id);

}
