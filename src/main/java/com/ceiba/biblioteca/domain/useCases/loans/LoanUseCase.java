package com.ceiba.biblioteca.domain.usecases.loans;

import com.ceiba.biblioteca.domain.dtos.request.LoanSaveRequest;
import com.ceiba.biblioteca.domain.dtos.response.LoanResponse;
import com.ceiba.biblioteca.domain.dtos.response.LoanSaveResponse;

public interface LoanUseCase {

    LoanSaveResponse save(LoanSaveRequest saveRequest);

    LoanResponse findById(Integer id);

}
