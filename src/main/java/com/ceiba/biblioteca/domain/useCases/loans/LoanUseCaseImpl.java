package com.ceiba.biblioteca.domain.usecases.loans;

import com.ceiba.biblioteca.domain.dtos.request.LoanSaveRequest;
import com.ceiba.biblioteca.domain.dtos.response.LoanResponse;
import com.ceiba.biblioteca.domain.dtos.response.LoanSaveResponse;
import com.ceiba.biblioteca.domain.exceptions.CustomException;
import com.ceiba.biblioteca.domain.exceptions.ResourceNotFoundException;
import com.ceiba.biblioteca.domain.mappers.LoanMapperDomain;
import com.ceiba.biblioteca.domain.models.loans.Book;
import com.ceiba.biblioteca.domain.models.loans.Loan;
import com.ceiba.biblioteca.domain.models.rules.Rule;
import com.ceiba.biblioteca.domain.models.users.User;
import com.ceiba.biblioteca.domain.models.users.UserType;
import com.ceiba.biblioteca.domain.repositories.LoanRepository;
import com.ceiba.biblioteca.domain.usecases.rules.RuleUseCase;
import com.ceiba.biblioteca.domain.usecases.users.UserTypeUseCase;
import com.ceiba.biblioteca.domain.usecases.users.UserUseCase;
import lombok.RequiredArgsConstructor;

import static com.ceiba.biblioteca.domain.utils.DomainConstants.MAXIMUM_LOAN_QUOTA_MSG;
import static com.ceiba.biblioteca.domain.utils.DomainConstants.RESOURCE_NOT_FOUND;


@RequiredArgsConstructor
public class LoanUseCaseImpl implements LoanUseCase {
    private final LoanRepository repository;
    private final BookUseCase bookUseCase;
    private final RuleUseCase ruleUseCase;
    private final UserUseCase userUseCase;
    private final UserTypeUseCase userTypeUseCase;

    @Override
    public LoanSaveResponse save(LoanSaveRequest request) {
        UserType userType = userTypeUseCase.findById(request.getTipoUsuario());
        Rule rule = ruleUseCase.findByUserTypeId(userType.getTypeId());
        Book book = bookUseCase.findByIsbn(request.getIsbn());
        User user = userUseCase.findByIdentificationNumber(request.getIdentificacionUsuario());
        verifyRuleMaxLoans(user, rule.getMaximumLoans());

        Loan loan = new Loan(book, user, userType, rule.getMaximumDaysLoan());
        return LoanMapperDomain.entityToSaveResponse(repository.save(loan));
    }

    @Override
    public LoanResponse findById(Integer id) {
        return LoanMapperDomain.entityToResponse(repository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format(RESOURCE_NOT_FOUND, id))));
    }

    private void verifyRuleMaxLoans(User user, Integer maxLoans) {
        if (repository.numberActiveLoans(user.getUserId()) >= maxLoans) {
            throw new CustomException(String.format(MAXIMUM_LOAN_QUOTA_MSG, user.getIdentificationNumber()));
        }
    }

}
