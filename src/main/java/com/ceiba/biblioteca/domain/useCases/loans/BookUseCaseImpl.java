package com.ceiba.biblioteca.domain.usecases.loans;

import com.ceiba.biblioteca.domain.exceptions.ResourceNotFoundException;
import com.ceiba.biblioteca.domain.models.loans.Book;
import com.ceiba.biblioteca.domain.repositories.BookRepository;
import com.ceiba.biblioteca.domain.utils.DomainConstants;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class BookUseCaseImpl implements BookUseCase {

    private final BookRepository repository;

    @Override
    public Book findById(Long id) {
        return repository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(String.format(DomainConstants.RESOURCE_NOT_FOUND, id)));
    }

    @Override
    public Book findByIsbn(String isbn) {
        return repository.findByIsbn(isbn)
                .orElseThrow(() -> new ResourceNotFoundException(
                        String.format(DomainConstants.RECORD_NOT_FOUND, "Libro", "isbn", isbn)));
    }

}
