package com.ceiba.biblioteca.domain.dtos.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDate;

@Data
public class LoanResponse {

    private Integer id;
    private String isbn;
    private String identificacionUsuario;
    private Integer tipoUsuario;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private LocalDate fechaMaximaDevolucion;

}