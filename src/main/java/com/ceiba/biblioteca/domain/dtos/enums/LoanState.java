package com.ceiba.biblioteca.domain.dtos.enums;

import lombok.Getter;

public enum LoanState {
    ACTIVE(0),
    FINALIZED(1),
    EXPIRED(2);

    @Getter
    private final int state;

    LoanState(int state) {
        this.state = state;
    }
}
