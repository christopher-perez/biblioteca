package com.ceiba.biblioteca.domain.dtos.request;

import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class LoanSaveRequest {

    @NotNull
    @NotEmpty
    @Size(max = 10)
    private String isbn;

    @NotNull
    @NotEmpty
    @Size(max = 10)
    private String identificacionUsuario;

    @Min(1)
    @Max(9)
    private Integer tipoUsuario;

}