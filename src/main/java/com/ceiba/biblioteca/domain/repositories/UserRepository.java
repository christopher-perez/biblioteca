package com.ceiba.biblioteca.domain.repositories;

import com.ceiba.biblioteca.domain.models.users.User;

import java.util.Optional;

public interface UserRepository {

    Optional<User> findById(Long id);

    Optional<User> findByIdentificationNumber(String id);
}
