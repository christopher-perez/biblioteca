package com.ceiba.biblioteca.domain.repositories;

import com.ceiba.biblioteca.domain.models.loans.Loan;

import java.util.Optional;

public interface LoanRepository {

    Loan save(Loan loan);

    Optional<Loan> findById(Integer id);

    Integer numberActiveLoans(Long userId);
}
