package com.ceiba.biblioteca.domain.repositories;

import com.ceiba.biblioteca.domain.models.users.UserType;

import java.util.Optional;

public interface UserTypeRepository {

    Optional<UserType> findById(Integer id);
}
