package com.ceiba.biblioteca.domain.repositories;

import com.ceiba.biblioteca.domain.models.rules.Rule;

import java.util.Optional;

public interface RuleRepository {

    Optional<Rule> findByUserType(Integer id);

    Optional<Rule> findById(Long id);
}
