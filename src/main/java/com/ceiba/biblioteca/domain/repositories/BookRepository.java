package com.ceiba.biblioteca.domain.repositories;

import com.ceiba.biblioteca.domain.models.loans.Book;

import java.util.Optional;

public interface BookRepository {

    Optional<Book> findById(Long id);

    Optional<Book> findByIsbn(String isbn);
}
