package com.ceiba.biblioteca.domain.exceptions;

public class CustomException extends RuntimeException {

    public CustomException(String message) {
        super(message);
    }

}
