package com.ceiba.biblioteca.domain.utils;

public class DomainConstants {

    private DomainConstants(){
    }


    public static final String MAXIMUM_LOAN_QUOTA_MSG =
            "El usuario con identificación %s ya tiene un libro prestado por lo cual no se le puede realizar otro préstamo";
    public static final String RESOURCE_NOT_FOUND = "El item con id %d no ha sido encontrado";
    public static final String RECORD_NOT_FOUND = "%s con %s %s no ha sido encontrado";
    public static final String USER_TYPE_NOT_FOUND = "Tipo de usuario no permitido en la biblioteca";

}
