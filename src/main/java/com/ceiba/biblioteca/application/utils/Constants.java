package com.ceiba.biblioteca.application.utils;

public class Constants {

    public static final String RESOURCE_ALREADY_EXISTS = "El item %s ya se encuentra registrado";
    public static final String REQUEST_NOT_READABLE = "Request no puede ser leida";
    public static final String GENERAL_ERROR_MESSAGE = "Ha ocurrido un error";
    public static final String BAD_FORMED_REQUEST = "Ha ocurrido un error";
    public static final String PROPERTY_ERROR_FORMAT = "La propiedad %s %s";

    private Constants() {
    }
}