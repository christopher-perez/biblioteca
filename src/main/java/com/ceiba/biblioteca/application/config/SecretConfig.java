package com.ceiba.biblioteca.application.config;

import com.ceiba.biblioteca.application.utils.DotenvUtil;
import com.ceiba.biblioteca.application.utils.SecretDBModel;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
public class SecretConfig {

    @Bean
    @Profile("local")
    public SecretDBModel getDbModelLocal() {
        return new SecretDBModel(DotenvUtil.get("user"),
                DotenvUtil.get("password"), DotenvUtil.get("host"),
                DotenvUtil.get("port"), DotenvUtil.get("dbName"),
                DotenvUtil.get("engine"));
    }

}