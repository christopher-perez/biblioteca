package com.ceiba.biblioteca.application.config;

import com.ceiba.biblioteca.domain.repositories.BookRepository;
import com.ceiba.biblioteca.domain.repositories.LoanRepository;
import com.ceiba.biblioteca.domain.repositories.RuleRepository;
import com.ceiba.biblioteca.domain.repositories.UserRepository;
import com.ceiba.biblioteca.domain.repositories.UserTypeRepository;
import com.ceiba.biblioteca.domain.usecases.loans.BookUseCase;
import com.ceiba.biblioteca.domain.usecases.loans.BookUseCaseImpl;
import com.ceiba.biblioteca.domain.usecases.loans.LoanUseCase;
import com.ceiba.biblioteca.domain.usecases.loans.LoanUseCaseImpl;
import com.ceiba.biblioteca.domain.usecases.rules.RuleUseCase;
import com.ceiba.biblioteca.domain.usecases.rules.RuleUseCaseImpl;
import com.ceiba.biblioteca.domain.usecases.users.UserTypeUseCase;
import com.ceiba.biblioteca.domain.usecases.users.UserTypeUseCaseImpl;
import com.ceiba.biblioteca.domain.usecases.users.UserUseCase;
import com.ceiba.biblioteca.domain.usecases.users.UserUseCaseImpl;
import com.ceiba.biblioteca.infrastructure.adapters.loans.book.BookDataJPA;
import com.ceiba.biblioteca.infrastructure.adapters.loans.book.BookDataRepository;
import com.ceiba.biblioteca.infrastructure.adapters.loans.loan.LoanDataJPA;
import com.ceiba.biblioteca.infrastructure.adapters.loans.loan.LoanDataRepository;
import com.ceiba.biblioteca.infrastructure.adapters.rules.rule.RuleDataJPA;
import com.ceiba.biblioteca.infrastructure.adapters.rules.rule.RuleDataRepository;
import com.ceiba.biblioteca.infrastructure.adapters.users.type.UserTypeDataJPA;
import com.ceiba.biblioteca.infrastructure.adapters.users.type.UserTypeDataRepository;
import com.ceiba.biblioteca.infrastructure.adapters.users.user.UserDataJPA;
import com.ceiba.biblioteca.infrastructure.adapters.users.user.UserDataRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

    @Bean
    public LoanRepository loanRepository(LoanDataJPA loanDataJPA) {
        return new LoanDataRepository(loanDataJPA);
    }

    @Bean
    public LoanUseCase loanUseCase(LoanRepository loansRepository, BookUseCase bookUseCase,
                                   UserUseCase userUseCase, UserTypeUseCase userTypeUseCase,
                                   RuleUseCase ruleUseCase) {
        return new LoanUseCaseImpl(loansRepository, bookUseCase, ruleUseCase,
                userUseCase, userTypeUseCase);
    }

    @Bean
    public BookRepository bookRepository(BookDataJPA bookDataJPA) {
        return new BookDataRepository(bookDataJPA);
    }

    @Bean
    public BookUseCase bookUseCase(BookRepository booksRepository) {
        return new BookUseCaseImpl(booksRepository);
    }

    @Bean
    public UserRepository userRepository(UserDataJPA userDataJPA) {
        return new UserDataRepository(userDataJPA);
    }

    @Bean
    public UserUseCase userUseCase(UserRepository usersRepository) {
        return new UserUseCaseImpl(usersRepository);
    }

    @Bean
    public UserTypeRepository userTypeRepository(UserTypeDataJPA userTypeDataJPA) {
        return new UserTypeDataRepository(userTypeDataJPA);
    }

    @Bean
    public UserTypeUseCase userTypeUseCase(UserTypeRepository userTypesRepository) {
        return new UserTypeUseCaseImpl(userTypesRepository);
    }

    @Bean
    public RuleRepository ruleRepository(RuleDataJPA ruleDataJPA) {
        return new RuleDataRepository(ruleDataJPA);
    }

    @Bean
    public RuleUseCase ruleUseCase(RuleRepository rulesRepository) {
        return new RuleUseCaseImpl(rulesRepository);
    }

}
