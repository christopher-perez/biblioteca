package com.ceiba.biblioteca.application.config;

import com.ceiba.biblioteca.application.utils.SecretDBModel;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@RequiredArgsConstructor
@Profile("!test")
public class PostgresConfig {

    private static final String JDBC_URI_PG = "jdbc:postgresql://%s:%s/%s";
    private static final String HIBERNATE_DIALECT = "hibernate.dialect";
    private static final String HIBERNATE_DIALECT_VALUE = "org.hibernate.dialect.PostgreSQLDialect";
    private static final String HIBERNATE_SHOW_SQL = "hibernate.show_sql";
    private static final String HIBERNATE_SHOW_SQL_VALUE = "true";
    private static final String HIBERNATE_DDL_AUTO = "hibernate.hbm2ddl.auto";
    private static final String HIBERNATE_DDL_AUTO_VALUE = "none";
    private static final String DATABASE_DRIVER_CLASS = "org.postgresql.Driver";
    private static final String DATABASE_DRIVER_CLASS_VALUE = "org.postgresql.Driver";
    private static final String PACKAGE_BASE = "com.ceiba.biblioteca";
    private static final String PACKAGE_DOMAIN = PACKAGE_BASE + ".infrastructure.adapters";
    private static final int POOL_SIZE_EXAMPLE = 10;

    private final SecretDBModel secretDBModel;

    @Bean
    public DataSource getDataSource() {
        HikariConfig config = new HikariConfig();
        config.setMaximumPoolSize(POOL_SIZE_EXAMPLE);
        config.setJdbcUrl(String.format(JDBC_URI_PG, secretDBModel.getHost(),
                secretDBModel.getPort(), secretDBModel.getDbname()));
        config.setUsername(secretDBModel.getUsername());
        config.setPassword(secretDBModel.getPassword());
        return new HikariDataSource(config);
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource) {
        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean =
                new LocalContainerEntityManagerFactoryBean();

        entityManagerFactoryBean.setDataSource(dataSource);
        entityManagerFactoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        entityManagerFactoryBean.setPackagesToScan(PACKAGE_DOMAIN);

        Properties jpaProperties = new Properties();

        jpaProperties.put(HIBERNATE_DIALECT, HIBERNATE_DIALECT_VALUE);
        jpaProperties.put(HIBERNATE_SHOW_SQL, HIBERNATE_SHOW_SQL_VALUE);
        jpaProperties.put(HIBERNATE_DDL_AUTO, HIBERNATE_DDL_AUTO_VALUE);
        jpaProperties.put(DATABASE_DRIVER_CLASS, DATABASE_DRIVER_CLASS_VALUE);
        entityManagerFactoryBean.setJpaProperties(jpaProperties);

        return entityManagerFactoryBean;
    }
}
