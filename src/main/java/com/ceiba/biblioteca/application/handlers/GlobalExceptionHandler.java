package com.ceiba.biblioteca.application.handlers;

import com.ceiba.biblioteca.domain.dtos.ErrorResponse;
import com.ceiba.biblioteca.domain.dtos.ExceptionResponse;
import com.ceiba.biblioteca.domain.exceptions.CustomException;
import com.ceiba.biblioteca.domain.exceptions.ResourceAlreadyExistsException;
import com.ceiba.biblioteca.domain.exceptions.ResourceNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.ArrayList;
import java.util.List;

import static com.ceiba.biblioteca.application.utils.Constants.GENERAL_ERROR_MESSAGE;
import static com.ceiba.biblioteca.application.utils.Constants.PROPERTY_ERROR_FORMAT;
import static com.ceiba.biblioteca.application.utils.Constants.REQUEST_NOT_READABLE;

@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<ErrorResponse> notReadableException(HttpMessageNotReadableException ex) {
        log.error(REQUEST_NOT_READABLE);
        return new ResponseEntity<>(
                new ErrorResponse("BAD_REQUEST", REQUEST_NOT_READABLE),
                HttpStatus.BAD_REQUEST
        );
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<ExceptionResponse> resourceNotFound(ResourceNotFoundException ex) {
        return new ResponseEntity<>(
                new ExceptionResponse(ex.getMessage()),
                HttpStatus.NOT_FOUND
        );
    }

    @ExceptionHandler(ResourceAlreadyExistsException.class)
    public ResponseEntity<ExceptionResponse> resourceAlreadyExists(ResourceAlreadyExistsException ex) {
        return new ResponseEntity<>(
                new ExceptionResponse(ex.getMessage()),
                HttpStatus.CONFLICT
        );
    }

    @ExceptionHandler(CustomException.class)
    public ResponseEntity<ExceptionResponse> customException(CustomException ex) {
        return new ResponseEntity<>(
                new ExceptionResponse(ex.getMessage()),
                HttpStatus.BAD_REQUEST
        );
    }

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<Object> handleAllExceptions(Exception ex) {
        log.error(ex.getMessage());

        return new ResponseEntity<>(
                new ErrorResponse("INTERNAL_SERVER_ERROR", GENERAL_ERROR_MESSAGE),
                HttpStatus.INTERNAL_SERVER_ERROR
        );
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex) {
        List<String> details = new ArrayList<>();
        for (FieldError error : ex.getBindingResult().getFieldErrors()) {
            details.add(String.format(PROPERTY_ERROR_FORMAT, error.getField(), error.getDefaultMessage()));
        }

        return new ResponseEntity<>(
                new ErrorResponse("BAD_REQUEST", details),
                HttpStatus.BAD_REQUEST
        );
    }

}
